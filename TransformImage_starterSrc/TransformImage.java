/*************************************************************
 * TransformImage program
 * Modded 013019@Sep
 * Further Modded 020719@LorenzoPedroza to allow right click rotation and scroll wheel scaling
 *************************************************************/
import java.awt.*;
import java.awt.event.*;
import java.applet.*;
import java.util.*;
import java.awt.geom.*;
import java.net.*;

public class TransformImage extends Applet implements MouseListener, MouseMotionListener, MouseWheelListener {
    //image variable
    private Image image;

    //identity transformation
    AffineTransform identity = new AffineTransform();

    //transform variables
    int rotation;
    float scale;
    int tX, tY;

    private URL getURL(String filename) {
        URL url = null;
        try {
            url = this.getClass().getResource(filename);
        }
        catch (Exception e) { }

        return url;
    }

    //applet init event
    public void init() {
        image = getImage(getURL("PedrozaL_Spaceship.png"));

		    rotation = 0;
		    scale = 1;
        tX = 320;
        tY = 240;

        //initialize the listeners
        addMouseListener(this);
        addMouseMotionListener(this);
        addMouseWheelListener(this);
    }

    //applet paint event
    public void paint(Graphics g) {
        //create an instance of Graphics2D
        Graphics2D g2d = (Graphics2D) g;

        //working transform object
        AffineTransform trans = new AffineTransform();

        //random number generator
        Random rand = new Random();

        //applet window width/height
        int width = getSize().width;
        int height = getSize().height;

        //fill the background with black
        g2d.setColor(Color.BLACK);
        g2d.fillRect(0, 0, getSize().width, getSize().height);

        //draw the image
        trans.setTransform(identity);

        //move, rotate, scale the image randomly
        trans.translate(tX, tY);
        trans.rotate(Math.toRadians(rotation));
        trans.scale(scale, scale);

        //draw the image
        g2d.drawImage(image, trans, this);
    }

    //handle mouse events
    public void mouseEntered(MouseEvent m) { }
    public void mouseExited(MouseEvent m) { }
    public void mouseReleased(MouseEvent m) { }
    public void mouseClicked(MouseEvent m) {  }
    public void mousePressed(MouseEvent m) { 
        switch(m.getButton())
        {
            case MouseEvent.BUTTON1:
                rotation -= 15;   //negative for moving counterclockwise like other program
                if (rotation < -360)
                    rotation = 0;
                repaint();
                break;
            
            case MouseEvent.BUTTON3:
                rotation += 15;   //positive for moving clockwise like other program
                if (rotation > 360)
                    rotation = 0;
                repaint();
                break;
        }
    } //rotate image

//handle mouse motion events
    public void mouseMoved(MouseEvent e) { 
        tX = e.getX();
        tY = e.getY();
        repaint();
    } //follow image
    public void mouseDragged(MouseEvent e) { }

    //handle mouse wheel event
    public void mouseWheelMoved(MouseWheelEvent e) {
        float scaleFactor = 0.3f;
        if(e.getWheelRotation() < 0 ) //negative scale flips the image; unwanted behaviour. So check before execution
            scale += scaleFactor;
        else if (e.getWheelRotation()>0 && scale > 0.1f) //don't let the image get so small it dissapears from screen
            scale -= scaleFactor;
        repaint();
    } //scale image
}
