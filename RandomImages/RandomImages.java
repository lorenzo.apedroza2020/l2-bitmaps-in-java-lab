/*************************************************************
 * Beginning Java Game Programming, 2nd Edition
 * by Jonathan S. Harbour
 * RandomImages program
 * Modified by Lorenzo Pedroza for L2 on 02/07/19
 *************************************************************/
import java.awt.*;
import java.applet.*;
import java.util.*;
import java.awt.geom.*;
import java.net.*;

public class RandomImages extends Applet {
    //image variable. Array so you can add more images if wanted by increasing elements
    private Image[] image = new Image[2];

    //identity transformation
    AffineTransform identity = new AffineTransform();

    private URL getURL(String filename) {
        URL url = null;
        try {
            url = this.getClass().getResource(filename);
        }
        catch (Exception e) { }

        return url;
    }

    //applet init event
    public void init() {
        image[0] = getImage(getURL("spaceship.png"));
        image[1] = getImage(getURL("PedrozaL_Spaceship.png"));
    }

    //applet paint event
    public void paint(Graphics g) {
        //create an instance of Graphics2D
        Graphics2D g2d = (Graphics2D) g;

        //working transform object
        AffineTransform trans = new AffineTransform();

        //random number generator
        Random rand = new Random();

        //applet window width/height
        int width = getSize().width;
        int height = getSize().height;

        //fill the background with black
        g2d.setColor(Color.BLACK);
        g2d.fillRect(0, 0, getSize().width, getSize().height);

        //draw the image multiple times
        for (int n = 0; n < 50; n++) {
            //draw the images
            for (Image spaceShip : image) { //for each loop
                double scale ;
                trans.setTransform(identity);
                //move, rotate, scale the image randomly
                trans.translate(rand.nextInt()%width, rand.nextInt()%height);
                trans.rotate(Math.toRadians(360 * rand.nextDouble()));
                scale = rand.nextDouble()+1;
                if(spaceShip.equals(image[1]))
                    scale = rand.nextDouble()/3 + 0.01; //make my giant spaceship image smaller
                trans.scale(scale, scale);
                g2d.drawImage(spaceShip, trans, this);
            } 
        }
    }
}
