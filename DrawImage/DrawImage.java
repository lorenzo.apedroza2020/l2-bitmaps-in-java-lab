/*************************************************************
 * Beginning Java Game Programming, 2nd Edition
 * by Jonathan S. Harbour
 * DrawImage program
 * Modified for exercise 2 020719@LorenzoPedroza
 *************************************************************/

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.AffineTransform;
import java.applet.*;
import java.net.*;

public class DrawImage extends Applet implements KeyListener{
    //image variable
    private Image image;
    private float scale = 1.0f;
    private AffineTransform idenity = new AffineTransform();

    private URL getURL(String filename) {
        URL url = null;
        try {
            url = this.getClass().getResource(filename);
        }
        catch (Exception e) { }
        return url;
    }

    //applet init event
    public void init() {
        image = getImage(getURL("PedrozaL_Spaceship.png"));
        addKeyListener(this);
    }

    //applet paint event
    public void paint(Graphics g) {
        //create an instance of Graphics2D
        Graphics2D g2d = (Graphics2D) g;

        AffineTransform trans = new AffineTransform(idenity);

        //fill the background with black
        g2d.setColor(Color.BLACK);
        g2d.fillRect(0, 0, getSize().width, getSize().height);
        trans.scale(scale, scale);

        //draw the image
        g2d.drawImage(image, trans, this);
    }

    @Override
    public void keyReleased(KeyEvent e) {}

    @Override
    public void keyPressed(KeyEvent e) {}

    @Override
    public void keyTyped(KeyEvent e){
        /*traditional way
        switch(e.getKeyCode()){ //need help with this. VK (Virtual keys do not work well)
            case KeyEvent.VK_PLUS: //some compact keyboards lack a dedicated '+' button. And hitting shift will yield the wrong the event. So just use equals.
                scale += 0.1f; 
                break;
            case KeyEvent.VK_MINUS:
                scale -= 0.1f;
                break;
        }
        repaint(); 
        */

        // Alternative for keyboards without a dedicated '+' buton
        switch(e.getKeyChar()){ //need help with this. VK (Virtual keys do not work well)
            case '+':
                scale += 0.1f;
                break;
            case '-':
                if(scale > 0.1f) //don't let the image get so small it dissapears from screen
                    scale -= 0.1f;
                break;
        }
        repaint(); 
        
    }
}
