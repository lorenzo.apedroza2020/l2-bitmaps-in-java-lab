/*************************************************************
 * Beginning Java Game Programming, 2nd Edition
 * by Jonathan S. Harbour
 * DrawImage program
 * Modified DrawImage for DrawImages 020719@LorenzoPedroza
 *************************************************************/

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.applet.*;
import java.net.*;

public class DrawImages extends Applet{
    //image variable
    private Image image;
    private float scale = 0.1f; //scale my large image down
    private AffineTransform identity = new AffineTransform();

    /*Translation Variables
    int translationX = 0;  //Noticed that at this scope, variables for translation are not working. mystery to me.
    int translationY = 0;
    */

    private URL getURL(String filename) {
        URL url = null;
        try {
            url = this.getClass().getResource(filename);
        }
        catch (Exception e) { }
        return url;
    }

    //applet init event
    public void init() {
        image = getImage(getURL("PedrozaL_Spaceship.png"));
    }

    //applet paint event
    public void paint(Graphics g) {
        //create an instance of Graphics2D
        Graphics2D g2d = (Graphics2D) g;

        //Working transform object
        AffineTransform trans = new AffineTransform();

        //fill the background with black
        g2d.setColor(Color.BLACK);
        g2d.fillRect(0, 0, getSize().width, getSize().height);

        int x = 0, y = 0;  //at this scope works. Weird. Nontheless works.
        for(int i = 0; i < 12; i++){
            for(int j = 0; j < 12 ; j++)
            {
                trans.setTransform(identity);
                trans.translate(x, y);
                trans.scale(scale, scale);
                g2d.drawImage(image, trans, this);
                x += 40;
            }
            x = 0;  //reset to draw next column
            y += 40;
        }        

    }
}


